---
# This is the entry point of your VuePress site, the layout will follow Default Them Config's Homepage unless you specify your own layout
home: true
heroImage: /hero.png
heroText: Hello
tagline: I am just another human who codes
# actionText: Get Started →
# actionLink: /guide/
features:
- title: I Code
  details: Minimal setup with markdown-centered project structure helps you focus on writing.
- title: I Deploy
  details: Enjoy the dev experience of Vue + webpack, use Vue components in markdown, and develop custom themes with Vue.
- title: I (try to) Design
  details: VuePress generates pre-rendered static HTML for each page, and runs as an SPA once a page is loaded.
# footer: MIT Licensed | Copyright © 2019 Christabella Adelina
---