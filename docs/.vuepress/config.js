module.exports = {
    title: 'Christabella Adelina',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/christabella.dev/',
    dest: 'public',
    themeConfig: {
        search: false,
        nav: [
            {text: 'Profile', link: '/profile/'},
            {text: 'Work', link: '/work/'},
            {text: 'Contact', link: '/contact/'},
            // {text: 'Blog', link: '/'},
        ]
    }   
}