# Contact

Why would you contact me?

- You have some questions for me
- You have an idea for a side project that you want to start with me
- You think I am a match for a work opportunity that you know of
- You feel lonely and you want to talk to somone <3

### Platform to contact:

1. Drop a note form
2. Link to social account
    - LinkedIn
    - Instagram
    - Github
    - Gitlab
