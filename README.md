<!-- ![Build Status](https://gitlab.com/pages/vuepress/badges/master/build.svg)

--- -->

Personal website that I have to build to apply for jobs and internships because everyone seems to be asking for a website even though I have no clue what I will put in the website

---

## Stack
If you need to whip up a personal website real quick like me, I would recommend this stack:
- VuePress
- GitLab Pages
- GitLab CI/CD
